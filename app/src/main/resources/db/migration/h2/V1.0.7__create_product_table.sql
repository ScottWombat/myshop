DROP TABLE IF EXISTS product;

CREATE TABLE product (
  id bigint(20) GENERATED ALWAYS AS IDENTITY,
  userId bigint(20) NOT NULL,
  title varchar(75) NOT NULL,
  metaTitle varchar(100) DEFAULT NULL,
  slug varchar(100)  NOT NULL,
  summary tinytext ,
  type smallint(6) NOT NULL DEFAULT 0,
  sku varchar(100)  NOT NULL,
  price float NOT NULL DEFAULT 0,
  discount float NOT NULL DEFAULT 0,
  quantity smallint(6) NOT NULL DEFAULT 0,
  shop tinyint(1) NOT NULL DEFAULT 0,
  createdAt timestamp NOT NULL,
  updatedAt timestamp DEFAULT NULL,
  publishedAt timestamp DEFAULT NULL,
  startsAt timestamp DEFAULT NULL,
  endsAt timestamp DEFAULT NULL,
  content text,
  UNIQUE KEY uq_slug (slug),
  UNIQUE KEY idx_product_user (userId)
)