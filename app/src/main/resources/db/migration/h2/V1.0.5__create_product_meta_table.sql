DROP TABLE IF EXISTS product_meta;
CREATE TABLE product_meta (
  id bigint(20) GENERATED ALWAYS AS IDENTITY,
  productId bigint(20) NOT NULL,
  key varchar(50) NOT NULL,
  content text,
  UNIQUE KEY uq_product_meta (productId,key),
  UNIQUE KEY idx_meta_product (productId)
)