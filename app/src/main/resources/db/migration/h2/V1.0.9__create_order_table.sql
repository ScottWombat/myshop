DROP TABLE IF EXISTS "ORDER";

CREATE TABLE "ORDER" (
  id bigint(20) GENERATED ALWAYS AS IDENTITY,
  userId bigint(20) NOT NULL,
  sessionId varchar(100)  NOT NULL,
  token varchar(100)  NOT NULL,
  status smallint(6) NOT NULL DEFAULT '0',
  subTotal float NOT NULL DEFAULT '0',
  itemDiscount float NOT NULL DEFAULT '0',
  tax float NOT NULL DEFAULT '0',
  shipping float NOT NULL DEFAULT '0',
  total float NOT NULL DEFAULT '0',
  promo varchar(50)  DEFAULT NULL,
  discount float NOT NULL DEFAULT '0',
  grandTotal float NOT NULL DEFAULT '0',
  firstName varchar(50)  DEFAULT NULL,
  middleName varchar(50)  DEFAULT NULL,
  lastName varchar(50)  DEFAULT NULL,
  mobile varchar(15)  DEFAULT NULL,
  email varchar(50)  DEFAULT NULL,
  line1 varchar(50)  DEFAULT NULL,
  line2 varchar(50)  DEFAULT NULL,
  city varchar(50)  DEFAULT NULL,
  province varchar(50)  DEFAULT NULL,
  country varchar(50)  DEFAULT NULL,
  createdAt timestamp NOT NULL,
  updatedAt timestamp DEFAULT NULL,
  content text ,
  PRIMARY KEY (id)
);

CREATE INDEX idx_order_user on "ORDER"(userId);


