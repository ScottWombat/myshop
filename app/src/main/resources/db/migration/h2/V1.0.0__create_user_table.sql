DROP TABLE IF EXISTS user;

CREATE TABLE user (
  id int GENERATED ALWAYS AS IDENTITY,
  firstName varchar(50),
  middleName varchar(50),
  lastName varchar(50),
  mobile varchar(15) DEFAULT NULL,
  email varchar(50),
  passwordHash varchar(32),
  admin smallint NOT NULL DEFAULT 0,
  vendor smallint NOT NULL DEFAULT 0,
  registeredAt timestamp NOT NULL,
  lastLogin timestamp DEFAULT NULL,
  intro text,
  profile text,
  PRIMARY KEY (id),
  UNIQUE KEY uq_mobile (mobile),
  UNIQUE KEY uq_email (email)
);