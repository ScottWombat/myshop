DROP TABLE IF EXISTS cart;

CREATE TABLE cart (
  id bigint(20) GENERATED ALWAYS AS IDENTITY,
  userId bigint(20) DEFAULT NULL,
  sessionId varchar(100)  NOT NULL,
  token varchar(100)  NOT NULL,
  status smallint(6) NOT NULL DEFAULT '0',
  firstName varchar(50)  DEFAULT NULL,
  middleName varchar(50)  DEFAULT NULL,
  lastName varchar(50)  DEFAULT NULL,
  mobile varchar(15)  DEFAULT NULL,
  email varchar(50)  DEFAULT NULL,
  line1 varchar(50)   DEFAULT NULL,
  line2 varchar(50) DEFAULT NULL,
  city varchar(50) DEFAULT NULL,
  province varchar(50)  DEFAULT NULL,
  country varchar(50)  DEFAULT NULL,
  createdAt timestamp NOT NULL,
  updatedAt timestamp DEFAULT NULL,
  content text,
  PRIMARY KEY (id)
);

CREATE INDEX idx_cart_user on cart(userId);