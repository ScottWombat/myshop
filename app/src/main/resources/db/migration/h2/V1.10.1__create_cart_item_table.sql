DROP TABLE IF EXISTS cart_item;
CREATE TABLE cart_item (
  id bigint(20) GENERATED ALWAYS AS IDENTITY,
  productId bigint(20) NOT NULL,
  cartId bigint(20) NOT NULL,
  sku varchar(100)  NOT NULL,
  price float NOT NULL DEFAULT '0',
  discount float NOT NULL DEFAULT '0',
  quantity smallint(6) NOT NULL DEFAULT '0',
  active tinyint(1) NOT NULL DEFAULT '0',
  createdAt datetime NOT NULL,
  updatedAt datetime DEFAULT NULL,
  content text ,
  PRIMARY KEY (id)
);

CREATE INDEX idx_cart_item_product on cart_item(productId);
CREATE INDEX idx_cart_item_cart on cart_item(cartId);