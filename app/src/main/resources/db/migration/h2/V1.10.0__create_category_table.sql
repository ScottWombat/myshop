DROP TABLE IF EXISTS category;

CREATE TABLE category (
  id bigint(20) GENERATED ALWAYS AS IDENTITY,
  parentId bigint(20) DEFAULT NULL,
  title varchar(75)  NOT NULL,
  metaTitle varchar(100)  DEFAULT NULL,
  slug varchar(100)  NOT NULL,
  content text ,
  PRIMARY KEY (id)
);

CREATE INDEX idx_category_parent on category(parentId);