DROP TABLE IF EXISTS tag;

CREATE TABLE tag (
  id bigint(20) GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
  title varchar(75) NOT NULL,
  metaTitle varchar(100) DEFAULT NULL,
  slug varchar(100) NOT NULL,
  content text 
); 