DROP TABLE IF EXISTS order_item;

CREATE TABLE order_item (
  id bigint(20) GENERATED ALWAYS AS IDENTITY,
  productId bigint(20) NOT NULL,
  orderId bigint(20) NOT NULL,
  sku varchar(100)  NOT NULL,
  price float NOT NULL DEFAULT 0,
  discount float NOT NULL DEFAULT 0,
  quantity smallint(6) NOT NULL DEFAULT 0,
  createdAt timestamp NOT NULL,
  updatedAt timestamp DEFAULT NULL,
  content text,
  PRIMARY KEY(id)
);
CREATE INDEX idx_order_item_product ON order_item(productId);
CREATE INDEX idx_order_item_order ON order_item(orderId);