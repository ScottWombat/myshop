import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { StorageMap } from '@ngx-pwa/local-storage';
import { AppState } from '@app/store/app.state';
import { getCount } from '@app/store/cart/counter.selector';
import {selectFeatureIsAuthenticated }  from '@app/store/auth/auth.select';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  auth$: Observable<boolean>
  count$: Observable<number>
  storage$: Observable<any>

  constructor(private store$: Store<AppState>, private storage: StorageMap) { 
    this.auth$ = store$.pipe(select(selectFeatureIsAuthenticated));
    this.count$ = this.store$.pipe(select(getCount))

  }

  ngOnInit(): void {
    this.auth$.subscribe(data => console.log(data));
  }
  /*
decrement(): void {
    this.store.dispatch(decrement());
  }
  increment(): void {
    this.store.dispatch(increment());
  }
  storeVal(num: number): void {
    this.store.dispatch(storeCounter({ val: num }));
  }
  ngOnInit(): void {
    this.increment();
  }
  */

}
