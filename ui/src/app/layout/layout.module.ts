import { NgModule,CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,  ReactiveFormsModule }   from '@angular/forms';
import {RouterModule} from '@angular/router';
import { HeaderComponent } from './header/header.component';
import { MenuComponent } from './menu/menu.component';
import { ContentComponent } from './content/content.component';
import { FooterComponent } from './footer/footer.component';
import { NgxSpinnerModule } from 'ngx-spinner';
@NgModule({
  declarations: [
    HeaderComponent,
    MenuComponent,
    ContentComponent,
    FooterComponent
  ],
  imports: [
    CommonModule,FormsModule,ReactiveFormsModule,RouterModule, NgxSpinnerModule,

  ],
  exports: [
      HeaderComponent,
      MenuComponent,
      ContentComponent,
      FooterComponent
   ],
   schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class LayoutModule { }
