import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from '@app/material'
@NgModule({
  declarations: [
    HomeComponent
  ],
  imports: [
    CommonModule, 
    NgxSpinnerModule, 
    BrowserModule, 
    BrowserAnimationsModule,
    MaterialModule
  ],
  exports: [
        HomeComponent
   ],
   schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class FeaturesModule { }
