import { Injectable } from "@angular/core";
import { Observable, of, throwError } from 'rxjs';
import { HttpClient } from "@angular/common/http";

import { map } from "rxjs/operators";

import { User,Credentials } from "./user";

@Injectable({ providedIn: "root" })
export class LoginService {
 // private currentUserSubject: BehaviorSubject<User>;
 // public currentUser: Observable<User>;

  constructor(private http: HttpClient) {
    //this.currentUserSubject = new BehaviorSubject<User>(
      //JSON.parse(localStorage.getItem("currentUser"))
    //);
   // this.currentUser = this.currentUserSubject.asObservable();
  }

  //public get currentUserValue(): User {
  //  return this.currentUserSubject.value;
  //}

  login({username, password}: Credentials): Observable<User> {
    if (username !== 'test' && password !== 'ngrx') {
      return throwError(() => 'Invalid username or password');
    }

    return of({ name: 'User' });
    // post to fake back end, this url will be handled there...
     /*
    return this.http
      .post<any>(`/users/authenticate`, { username, password })
      .pipe(
        map(user => {
          // store user details and basic auth credentials in local storage to keep user logged in between page refreshes
          //user.authdata = window.btoa(username + ":" + password);
         // localStorage.setItem("currentUser", JSON.stringify(user));
          //this.currentUserSubject.next(user);
          return user;
        })
      );
    */
   //return new User({});
  }

  logout() {
    // remove user from local storage to log user out
    //localStorage.removeItem("currentUser");
    //this.currentUserSubject.next(null);
    return of(true)
  }
}
