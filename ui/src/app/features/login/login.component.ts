import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { AppState } from '@app/store/app.state';
import { LOGIN_USER } from '@app/store/login/login.actions'

import { first } from 'rxjs/operators';
import { LoginService } from './login.service';

@Component({ 
    templateUrl: 'login.component.html',
    styleUrls: ['login.component.scss']
})
export class LoginComponent implements OnInit {
    credential: Credential;
    loginForm: FormGroup;
    loading = false;
    submitted = false;
    returnUrl: string;
    error = '';

    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private store: Store<AppState>
    ) {}

    ngOnInit() {
        this.loginForm = this.formBuilder.group({
            username: ['', Validators.required],
            password: ['', Validators.required]
        });


        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    }

    ngAfterViewInit(){
        //this.loginForm.reset();
    }

    // convenience getter for easy access to form fields
    get f() { return this.loginForm.controls;}
    onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.loginForm.invalid) {
            return;
        }

        this.loading = true;
        console.log(this.loginForm.value.username);
        console.log(this.loginForm.value.password);
        console.log("DDD" + this.returnUrl);
       // this.credential = {username: this.loginForm.value.username,password: this.loginForm.value.password};
        this.store.dispatch(LOGIN_USER({username: this.loginForm.value.username,password: this.loginForm.value.password}));
        //this.router.navigate([this.returnUrl]);
        //this.authenticationService.login('test', 'test');
        //this.authenticationService.login('test', 'test')
        //    .pipe(first())
        //    .subscribe(
        //        data => {
        //            this.router.navigate([this.returnUrl]);
        //        },
        //        error => {
        //            this.error = error;
        //            this.loading = false;
        //        });
    }
}

