import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store, select } from '@ngrx/store';
import {selectFeatureIsAuthenticated }  from '@app/store/auth/auth.select';
import { AppState } from '@app/store/app.state';
import { init } from '@app/store/spinner/spinner.actions';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  auth$: Observable<boolean>

  constructor(private store$: Store<AppState>) {
      this.auth$ = store$.pipe(select(selectFeatureIsAuthenticated));

   }

  ngOnInit(): void {
    this.auth$.subscribe(data => console.log(data));
    this.store$.dispatch(init());
     /*
     this.spinner.show();
     setTimeout(() => {
      this.spinner.hide();
    }, 5000);
    */
  
  }

}
