import { SpinnerEffects } from '@app/store/spinner/spinner.effects';
import { LoginEffects } from '@app/store/login/login.effect';

export const AppEffects = [SpinnerEffects, LoginEffects];

//export * from '@app/store/login/login.effect';