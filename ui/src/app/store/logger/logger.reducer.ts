import { Action, createReducer, on } from '@ngrx/store';
import { LoggerState } from './logger.state';
import * as fromLoginActions  from './logger.actions';

const reducer = createReducer<LoggerState>(
  { messages: [] },
  on(fromLoginActions.LOG_INFO, (state, { message }) => ({
    ...state,
    infoMessages: [...state.messages, message]
  }))
);

export function loggerReducer(
  state: LoggerState | undefined,
  action: Action) {
  return reducer(state, action);
}