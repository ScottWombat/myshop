import { createAction,props } from '@ngrx/store';

export const LOG_INFO = createAction('[LOGGER] Log message', props<{message: string}>());

/*
export const fromLoginActions = {
    LOG_INFO
}
*/