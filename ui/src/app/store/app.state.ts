import { LoginState } from './login/login.state';
import { AuthState } from './auth/auth.state'
import { LoggerState } from './logger/logger.state';
import { CounterState } from './cart/counter.state';
//import { ShoppingState } from './cart/shopping.state';
export interface AppState {
    auth: AuthState;
    login: LoginState;
    logger: LoggerState;
    counter: CounterState;
    //shopping: ShoppingState;
}