import { createSelector } from '@ngrx/store';
import { AppState } from '@app/store/app.state';
import { AuthState } from './auth.state';

export const selectAuthState = (state: AppState) => state.auth;

export const selectFeatureIsAuthenticated = createSelector(
    selectAuthState,
    (state: AuthState) => state.isAuthenticated
);