import {createAction,props } from '@ngrx/store';

export const LOGIN = createAction('[Auth] Login');
export const LOGOUT = createAction('[Auth] Logout');
/*
export const AuthActionTypes = {
  LOGIN: '[Auth] Login',
  LOGOUT: '[Auth] Logout'
};

export class ActionAuthLogin implements Action {
  readonly type = AuthActionTypes.LOGIN;
}

export class ActionAuthLogout implements Action {
  readonly type = AuthActionTypes.LOGOUT;
}

export type AuthActions = ActionAuthLogin | ActionAuthLogout;
*/