import { Action, createReducer, on } from '@ngrx/store';
import { AuthState } from './auth.state';
//import { AuthActions, AuthActionTypes } from './auth.actions';
import { LOGIN, LOGOUT } from './auth.actions';
export const initialState: AuthState = {
  isAuthenticated: false
};
const createAuthReducer = createReducer(
    initialState,
    on(LOGIN,state => ({ ...state, isAuthenticated: true})),
    on(LOGOUT,state => ({ ...state, isAuthenticated: false}))
);
export function authReducer( state: AuthState | undefined,action: Action) {
  return createAuthReducer(state, action);
}
/*
export function authReducer( state: AuthState = initialState,action: AuthActions): AuthState {

  switch (action.type) {
    case AuthActionTypes.LOGIN:
      return { ...state, isAuthenticated: true };

    case AuthActionTypes.LOGOUT:
      return { ...state, isAuthenticated: false };

    default:
      return state;
  }
}
*/

