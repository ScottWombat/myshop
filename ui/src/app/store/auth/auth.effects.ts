import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Action } from '@ngrx/store';
import { Actions, createEffect, Effect, ofType } from '@ngrx/effects';
import { tap } from 'rxjs/operators';

import { LocalStorageService } from '@app/local-storage/local-storage.service';

import {
  LOGIN,
  LOGOUT
} from './auth.actions';

export const AUTH_KEY = 'AUTH';

@Injectable()
export class AuthEffects {
  constructor(
    private actions$: Actions<Action>,
    private localStorageService: LocalStorageService,
    private router: Router
  ) {}

  login = createEffect(() =>this.actions$.pipe(
    ofType(LOGIN),
    tap(() =>
      this.localStorageService.setItem(AUTH_KEY, { isAuthenticated: true })
    )
  ));


  logout = createEffect(() =>this.actions$.pipe(
    ofType(LOGOUT),
    tap(() => {
      this.router.navigate(['']);
      this.localStorageService.setItem(AUTH_KEY, { isAuthenticated: false });
    })
  ));
}