import {
    ActionReducer,
    ActionReducerMap,
    createFeatureSelector,
    createSelector,
    MetaReducer
  } from '@ngrx/store';

  import { AppState } from './app.state';
  import { authReducer } from './auth/auth.reducer';
  import { loginReducer } from './login/login.reducer';
  import { counterReducer } from './cart/counter.reducer';
  import { loggerReducer } from './logger/logger.reducer';

  export const reducers: ActionReducerMap<AppState> = {
    auth: authReducer,
    login: loginReducer,
    counter: counterReducer,
    logger: loggerReducer
  };