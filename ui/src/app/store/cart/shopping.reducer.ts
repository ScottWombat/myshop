import { createReducer, on } from '@ngrx/store';
import { ShoppingState } from './shopping.state';
import * as ShoppingAction from './shopping.action';
const initialState: ShoppingState = {
    items: [],
    loading: false
  };
  
  export const ShoppingReducer = createReducer(
    initialState,
    on(ShoppingAction.LoadShoppingAction, state => ({
      ...state,
      loading: true
    })),
    on(ShoppingAction.LoadShoppingSuccessAction, (state, { list }) => ({
      ...state,
      list,
      loading: false
    })),
    on(ShoppingAction.LoadShoppingFailureAction, (state, { error }) => ({
      ...state,
      error
    })),
    on(ShoppingAction.AddItemAction, state => ({
      ...state,
      loading: true
    })),
    on(ShoppingAction.AddItemSuccessAction, (state, { item }) => ({
      ...state,
      list: [...state.items, item],
      loading: false
    })),
    on(ShoppingAction.AddItemFailureAction, (state, { error }) => ({
      ...state,
      error
    })),
    on(ShoppingAction.DeleteItemAction, state => ({
      ...state,
      loading: true
    })),
    on(ShoppingAction.DeleteItemSuccessAction, (state, { id }) => ({
      ...state,
      list: state.items.filter(item => item.id !== id),
      loading: false
    })),
    on(ShoppingAction.DeleteItemFailureAction, (state, { error }) => ({
      ...state,
      error,
      loading: false
    }))
  );
  