import { Action, createReducer, on } from '@ngrx/store';
import { increment, decrement } from '@app/store/cart/counter.actions';
import { CounterState } from './counter.state';

export const initialState: CounterState = {
  count: 10
};

const createCounterReducer = createReducer(
  initialState,
  on(increment, state => ({ ...state, count: state.count + 1 })),
  on(decrement, state => ({ ...state, count: state.count - 1 }))
);

export function counterReducer(state: CounterState | undefined, action: Action) {
  return createCounterReducer(state, action);
}
