import { ShoppingItem } from './shopping-item.model';

export interface ShoppingState{
    items: ShoppingItem[],
    loading: boolean
 }