import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Action, createReducer, on } from '@ngrx/store';
import { LoginState } from './login.state';
//import { LOGIN_USER, LOGIN_SUCCESS, LOGIN_FAILURE } from './login.actions';
import { fromLoginActions } from '@app/store/login/login.actions'

export const initialState: LoginState = {
   user: {id:0,username:'rev',role:'admin'}
  };
  const createLoginReducer = createReducer(
      initialState,
      on(fromLoginActions.LOGIN_USER,state => ({ ...state})),
      on(fromLoginActions.LOGIN_SUCCESS,state => ({ ...state, isAuthenticated: false})),
     // on(LOGIN_FAILURE,state => ({ ...state, isAuthenticated: false}))
  );
  export function loginReducer( state: LoginState | undefined,action: Action) {
    return createLoginReducer(state, action);
  }