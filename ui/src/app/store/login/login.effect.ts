import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Action } from '@ngrx/store';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { tap,exhaustMap, mergeMap,map,switchMap,catchError } from 'rxjs/operators';
import { AuthenticationService } from '@app/services/authentication.service';
import { LOGIN,LOGOUT } from '@app/store/auth/auth.actions'
import { Credential } from '@app/models/credential.model';
import { StorageMap } from '@ngx-pwa/local-storage'
import { Store } from '@ngrx/store';
import { AppState } from '@app/store/app.state';
import {
  LOGIN_FAILURE,
  LOGIN_SUCCESS,
  LOGIN_USER
} from './login.actions';
import { Observable } from 'rxjs';

export const AUTH_KEY = 'AUTH';

@Injectable()
export class LoginEffects {
  constructor(
    private store: Store<AppState>,
    private actions$: Actions<Action>,
    private authenticationService: AuthenticationService,
    private storage: StorageMap,
    private router: Router
  ) {}


  login_user$:  Observable<any> = createEffect( 
    () => this.actions$.pipe(
      ofType(LOGIN_USER),
      //switchMap(({username,password}) =>
   
      switchMap((action) => 
        this.authenticationService.authenticate(action.username,action.password).pipe(
          map((res:Credential) =>{
              console.log('ll');
              this.store.dispatch(LOGIN());
              //this.router.navigate(['/']);
          }),
          tap(() => {
            this.router.navigate(['/']);
          }),
          catchError(error => { throw 'error in sosurce'})
        )
  
        )
    
      ), { dispatch:false}
  );

  login_success$ = createEffect(() => this.actions$.pipe(
      ofType(LOGIN_SUCCESS),
      tap(() => {
        console.log('login_success');
        this.router.navigate(['/']);
      })
    ),{dispatch:false}
  );

  login_failure$ = createEffect(() => this.actions$.pipe(
    ofType(LOGIN_FAILURE),
    tap(() => {
      console.log('login_failuew');
      this.router.navigate(['/']);
    })
  ),{dispatch:false}
);



  /*
  logout = createEffect(() =>this.actions$.pipe(
    ofType(LOGOUT),
    tap(() => {
      this.router.navigate(['']);
      this.localStorageService.setItem(AUTH_KEY, { isAuthenticated: false });
    })
  ));
  */
}