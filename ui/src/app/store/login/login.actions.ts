import { createAction,props } from '@ngrx/store';
import { Credential } from '@app/models';

export const LOGIN_USER = createAction('[Login] Login User', props<{username: string,password: string}>());
export const LOGIN_SUCCESS = createAction('[Login] Logout Success',props<{data:any}>());
export const LOGIN_FAILURE = createAction('[Login] Logout Failure');

export const fromLoginActions = {
    LOGIN_USER,
    LOGIN_SUCCESS,
    LOGIN_FAILURE
}