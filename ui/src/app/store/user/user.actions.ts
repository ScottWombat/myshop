import { Action } from '@ngrx/store';

export const UserActionsTypes = {
    USER_LOGIN_TYPE: '[USER] LOGIN',
    USER_LOGOUT_TYPE: '[USER] LOGOUT'
}

export class UserLoginAction implements Action{
    type= UserActionsTypes.USER_LOGIN_TYPE;
    constructor(public payload:any){}
}

export class UserLogoutAction implements Action {
    type= UserActionsTypes.USER_LOGOUT_TYPE
    constructor(public payload:any) {}
}

export type UserActions = 
    | UserLoginAction
    | UserLogoutAction;