import { Params } from '@angular/router';
export interface RouterUrlState {
    url: string;
    params: Parames;
    queryParams: Params;
}