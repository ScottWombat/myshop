import { Injectable } from '@angular/core';
import { createEffect, Actions, ofType} from '@ngrx/effects';
import { mergeMap, delay, takeUntil } from 'rxjs/operators';
import { select, Store } from '@ngrx/store';
import {
    tap,
    concatMap,
    exhaustMap,
    map,
    withLatestFrom
  } from 'rxjs/operators';
import { init } from './spinner.actions';
import { NgxSpinnerService } from 'ngx-spinner';

@Injectable()
export class SpinnerEffects {

    constructor(
        private actions$: Actions,
        private spinner: NgxSpinnerService,
        private store$: Store
    ) {}

    init$ = createEffect(() =>
        this.actions$.pipe(
            ofType(init),
            tap(() => this.spinner.show()),
            delay(2000),
            tap(() => this.spinner.hide())
        ),
        { dispatch: false }
    );
  
}