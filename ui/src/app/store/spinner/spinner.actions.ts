import { createAction, props } from '@ngrx/store';

export const init = createAction('[Random] Init');
export const showSpinner = createAction('[UI] Show Spinner');
export const hideSpinner = createAction('[UI] Hide Spinner');

