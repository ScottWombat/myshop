import { Injectable } from '@angular/core';

import { User } from '@app/models';

@Injectable({
    providedIn: 'root',
})
export class AuthService {
      currentUser: User | null;
      redirectUrl: string;
  
      constructor() { 
          this.currentUser = {id: 1};

       }
  
      isLoggedIn(): boolean {
          return !!this.currentUser;
      }
  
      login(userName: string, password: string): void {
          // Code here would log into a back end service
          // and return user information
          // This is just hard-coded here.
          this.currentUser = {
              id: 2,
              username: 're',
              role: 'admin'
          };
      }
  
      logout(): void {
          this.currentUser = null;
      }
}