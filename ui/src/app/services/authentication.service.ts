import { Injectable } from '@angular/core';
import { Credential } from '@app/models';
import { Observable, throwError,of, BehaviorSubject } from 'rxjs';
import { map,catchError,delay,tap} from 'rxjs/operators'
import { HttpClient,HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { User } from '@app/models';
import { StorageMap } from '@ngx-pwa/local-storage'
@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
    
    //private credentialSubject: BehaviorSubject<Credential>;
    //private credential: Observable<Credential>;
    API_URL="api/credentials";
    ID=1;
    NAME='test'
    currentUser: User | null

    constructor(private http: HttpClient,private storage: StorageMap){}

    authenticate(username: string, password: string): Observable<Credential>{
  
        return this.http.get<Credential>(`${this.API_URL}/?id=${this.ID}`,this.httpOptions).pipe(
                tap(credential => console.log(JSON.stringify(credential))),
                map(credential => {
                  this.storage.set('credential',JSON.stringify(credential))
                  return credential;
                }),
                catchError(this.handleError)
                );
    }

    isLoggedIn(): boolean {
        return !!this.currentUser;
    }

    authenticate_add(username: string,password: string): Observable<Credential>{
        return this.http.post<Credential>('api/credentials',{username,password}).pipe(
                tap(data => console.log(JSON.stringify(data))),
                catchError(this.handleError)
                );
    }

    private handleError1(error: HttpErrorResponse): Observable<never> {
        console.error(error);
        return throwError(() => error);    
    }

    httpOptions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };
    handleError(error: HttpErrorResponse) {
        let errorMessage = 'Unknown error!';
        if (error.error instanceof ErrorEvent) {
          // Client-side errors
          errorMessage = `Error: ${error.error.message}`;
        } else {
          // Server-side errors
          errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
        }
        //window.alert(errorMessage);
        return throwError(() => window.alert(errorMessage));
      }
}