import { Injectable } from '@angular/core';
import { User } from '@app/models';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators'
import { HttpClient,HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  usersUrl = 'api/users';
  constructor(private httpClient: HttpClient) {}

  getUsers(): Observable<User[]> {
    return this.httpClient.get<User[]>('/api/users');
  }

  getUser(id: number): Observable<User> {
    const url = `${this.usersUrl}/${id}`;
    return this.httpClient.get<User>(url).pipe(
      catchError(this.handleError)
    )
  }

  /*
addVillain(name: string, episode: string): Observable<Villain> {
    const villain = { name, episode };

    return this.http.post<Villain>(this.villainsUrl, villain, cudOptions).pipe(
      catchError(this.handleError)
    );
  }

  deleteVillain(villain: number | Villain): Observable<Villain> {
    const id = typeof villain === 'number' ? villain : villain.id;
    const url = `${this.villainsUrl}/${id}`;

    return this.http.delete<Villain>(url, cudOptions).pipe(
      catchError(this.handleError)
    );
  }

  searchVillain(term: string): Observable<Villain[]> {
    term = term.trim();
    // add safe, encoded search parameter if term is present
    const options = term ?
    { params: new HttpParams().set('name', term)} : {};

    return this.http.get<Villain[]>(this.villainsUrl, options).pipe(
      catchError(this.handleError)
    );
  }

  updateVillain(villain: Villain): Observable<Villain> {
    return this.http.put<Villain>(this.villainsUrl, villain, cudOptions).pipe(
      catchError(this.handleError)
    );
  }

  */

  private handleError(error: HttpErrorResponse): Observable<never> {
    console.error(error);
    return throwError(() => error);    
  }
}
