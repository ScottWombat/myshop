import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, delay, tap } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class MockApiService {
  constructor(private http: HttpClient) {}

}