import { InMemoryDbService } from 'angular-in-memory-web-api';
import { User,Credential, Attendee } from '@app/models'
export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const attendees = [
      {
        id: 1,
        name: 'Duncan In Memory',
        attending: true,
        guests: 0
      }
    ] as Attendee[];

    const credentials = [
      {
        id: 1,
        username: 'test',
        password: 'test'
      }
    ] as Credential[];

    const users = [
      {
        id: 1,
        username: 'revit',
        role: 'admin'
      }
    ] as User[];

    return { 
      users: users,
      attendees: attendees, 
      credentials: credentials
    };
  }
}
