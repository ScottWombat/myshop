import { NgModule } from '@angular/core';
import { FormsModule,  ReactiveFormsModule }   from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from '@app/features/home/home.component';
import { LoginComponent } from './features/login/login.component';
import { AuthGuard } from '@app/services'
const routes: Routes = [
 {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    component: HomeComponent
  },
  {
      path: 'login',
      canActivate: [AuthGuard],
      loadChildren: () => import('./features/login/login.module').then(m => m.LoginModule)
    },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
